from oix.helpers.input import get_coords
from oix.board import ColorBoard
from oix.board import Board
from oix.model import Model




class Game:
    def __init__(self):
        print('tworzę grę')
        self.model = Model()

    def choose_board(self):
        board = input('wybierz tablicę: 1 - Board, 2 - ColorBoard : ')

        if board == '1':
            self.board = Board()
        else:
            self.board = ColorBoard()


    def swich_player(self, current_player):
        if current_player == 'x':
            return 'o'
        return 'x'

    def run(self):
        print('uruchamiam grę')


        current_player = 'x'

        while True:
            self.board.print()

            # pobierz wspolrzedne
            print('Ruch gracza', current_player)

            #sprawdza czy puste

            while True:

                coords = get_coords()
                row = coords[0]
                col = coords[1]

                if self.model.is_free(row, col):
                    break
                print('pole zajęte')


            # postaw znak
            self.model.put_mark(current_player, row, col)
            self.board.put_mark(current_player, row, col)

            #sprawdz czy wygral
            winner = self.model.get_winner()
            if winner != None:
                self.board.print()  #pokazuje finalny wynik na tablicy
                print('zwycięzca', winner)
                break
            # elif winner == self.model.is_over()

            # przelacz gracza
            current_player = self.swich_player(current_player)

            if self.model.is_over():
                print('nikt nie wygrał, koniec gry')
                break





def main():
    game = Game()
    game.choose_board()
    game.run()

if __name__ == '__main__':
    main()