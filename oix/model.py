class Model:
    def __init__(self):
        self.matrix = [
            [None, None, None],
            [None, None, None],
            [None, None, None],
        ]

    def is_free(self, row, col):
         return self.matrix[row][col] is None
    #przemyśl

    def is_over(self):

        if self.get_winner() is None:
            for row in self.matrix:
                for item in row:
                    if item is None:
                        return False
            return True



    def get_row_winner(self):
        for row in self.matrix:
            if row[0] == row[1] and row[1] == row[2] and row[0] is not None:
                return row[0]
        return None


    def get_col_winner(self):
        for index in range(len(self.matrix)):
            result = [self.matrix[0][index], self.matrix[1][index], self.matrix[2][index]]
            if result[0] == result[1] and result[1] == result[2] and result[0] is not None:
                return result[0]
        return None


    def get_left_diagonal_winner(self):
        if self.matrix[0][0] != self.matrix[1][1] or self.matrix[1][1] != self.matrix[2][2]:
            return None
        return self.matrix[0][0]

    def get_right_diagonal_winner(self):
        if self.matrix[2][0] != self.matrix[1][1] or self.matrix[1][1] != self.matrix[0][2]:
            return None
        return self.matrix[2][0]




    def get_winner(self):
        winner = self.get_row_winner()
        if winner != None:
            return winner

        winner = self.get_col_winner()
        if winner != None:
            return winner

        winner = self.get_left_diagonal_winner()
        if winner != None:
            return winner

        winner = self.get_right_diagonal_winner()
        if winner != None:
            return winner

        return None






    def put_mark(self, mark, row, col):
        self.matrix[row][col] = mark



#sprawdzenie

if __name__ == '__main__':

    model = Model()

    model.put_mark('x', 0, 1)
    model.put_mark('x', 1, 1)
    model.put_mark('x', 2, 1)


    print('winner:', model.get_winner())
    print('koniec:',model.is_over())

