from termcolor import colored

class Board:
    def __init__(self):
        self.matrix = [
            [' ', ' ', ' '],
            [' ', ' ', ' '],
            [' ', ' ', ' '],
        ]

    def print(self):
        for line in self.matrix:
            # print(colored(''.join(line), 'blue'))  # koloruje tablice
            print(line)

    def put_mark(self, mark, row, col):
        self.matrix[row][col] = mark




class ColorBoard:
    def __init__(self):
        self.matrix = [
            [' ', '╔', '═', '═', '═', '╤', '═', '═', '═', '╤', '═', '═', '═', '╗'],
            ['1', '║', ' ', ' ', ' ', ' │', ' ', ' ', ' ', ' │', ' ', ' ', ' ', ' ║'],
            [' ', '╟', '─', '─', '─', '┼', '─', '─', '─', '┼', '─', '─', '─', '╢'],
            ['2', '║', ' ', ' ', ' ', ' │', ' ', ' ', ' ', ' │', ' ', ' ', ' ', ' ║'],
            [' ', '╟', '─', '─', '─', '┼', '─', '─', '─', '┼', '─', '─', '─', '╢'],
            ['3', '║', ' ', ' ', ' ', ' │', ' ', ' ', ' ', ' │', ' ', ' ', ' ', ' ║'],
            [' ', '╚', '═', '═', '═', '╧', '═', '═', '═', '╧', '═', '═', '═', '╝'],
            [' ', ' ', ' ', ' A', ' ', ' ', ' ', ' B', ' ', ' ', ' ', ' C', ' ', ' '],
        ]


    def print(self):
        for line in self.matrix:
            print(colored(''.join(line), 'blue'))   #koloruje tablice

    def put_mark(self, mark, row, col):
        row = 2 * row + 1
        col = 4 * col + 3
        self.matrix[row][col] = mark


#zrobić board do wyboru

