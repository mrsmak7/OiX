import unittest

from oix.model import Model


class TestIsOver(unittest.TestCase):

    def test_all_cells_empty(self):
        model = Model()

        result = model.is_over()

        self.assertEqual(result, False)


    def test_all_cells_full(self):
        model = Model()
        model.put_mark('x', 0, 0)
        model.put_mark('o', 0, 1)
        model.put_mark('x', 0, 2)
        model.put_mark('o', 1, 0)
        model.put_mark('x', 1, 2)
        model.put_mark('o', 1, 1)
        model.put_mark('x', 2, 0)
        model.put_mark('o', 2, 2)
        model.put_mark('x', 2, 1)


        result = model.is_over()

        self.assertEqual(result, True)


    def test_8_moves(self):

        model = Model()
        model.put_mark('x', 0, 0)
        model.put_mark('o', 1, 0)
        model.put_mark('x', 0, 2)
        model.put_mark('o', 1, 0)
        model.put_mark('x', 1, 2)
        model.put_mark('o', 1, 1)
        model.put_mark('x', 2, 0)
        model.put_mark('o', 2, 2)

        result = model.is_over()

        self.assertEqual(result, False)

    def test_xxx_moves(self):
        model = Model()
        model.put_mark('x', 0, 0)

        result = model.is_over()

        self.assertEqual(result, False)


class TestGetWinner(unittest.TestCase):

    #wszystkie pola

    def test_empty_board(self):
        model = Model()

        result = model.get_winner()

        self.assertEqual(result, None)

    def test_full_board_no_one_win(self):

        model = Model()
        model.put_mark('x', 1, 0)
        model.put_mark('o', 0, 0)
        model.put_mark('x', 0, 1)
        model.put_mark('o', 0, 2)
        model.put_mark('x', 1, 2)
        model.put_mark('o', 1, 1)
        model.put_mark('x', 2, 0)
        model.put_mark('o', 2, 1)
        model.put_mark('x', 2, 2)

        result = model.get_winner()

        self.assertEqual(result, None)


    #wiersze

    def test_x_left_corner(self):

        model = Model()
        model.put_mark('x', 0, 0)

        result = model.get_winner()

        self.assertEqual(result, None)


    def test_x_middle_win(self):

        model = Model()
        model.put_mark('x', 1, 0)
        model.put_mark('x', 1, 1)
        model.put_mark('x', 1, 2)

        result = model.get_winner()

        self.assertEqual(result, 'x')



    def test_x_empty_x_middle(self):

        model = Model()
        model.put_mark('x', 1, 0)
        model.put_mark('x', 1, 2)

        result = model.get_winner()

        self.assertEqual(result, None)

    def test_x_down_win(self):

        model = Model()
        model.put_mark('x', 2, 0)
        model.put_mark('o', 1, 0)
        model.put_mark('x', 1, 1)
        model.put_mark('x', 2, 1)
        model.put_mark('o', 1, 2)
        model.put_mark('x', 2, 2)

        result = model.get_winner()

        self.assertEqual(result, 'x')


    def test_x_win_middle(self):

        model = Model()
        model.put_mark('x', 1, 0)
        model.put_mark('x', 1, 1)
        model.put_mark('x', 1, 2)

        result = model.get_winner()

        self.assertEqual(result, 'x')


    #kolumny

    def test_o_win_middle_col(self):

        model = Model()
        model.put_mark('o', 0, 1)
        model.put_mark('o', 1, 1)
        model.put_mark('o', 2, 1)

        result = model.get_winner()

        self.assertEqual(result, 'o')

    def test_o_right_col_win(self):

        model = Model()
        model.put_mark('o', 0, 2)
        model.put_mark('x', 0, 1)
        model.put_mark('o', 1, 2)
        model.put_mark('x', 2, 1)
        model.put_mark('o', 1, 1)
        model.put_mark('o', 2, 2)

        result = model.get_winner()

        self.assertEqual(result, 'o')

    def test_o_empty_o_middle(self):

        model = Model()
        model.put_mark('o', 0, 1)
        model.put_mark('o', 2, 1)

        result = model.get_winner()

        self.assertEqual(result, None)


    def test_oxo_middle_col(self):
        model = Model()
        model.put_mark('o', 0, 1)
        model.put_mark('x', 1, 1)
        model.put_mark('o', 2, 1)

        result = model.get_winner()

        self.assertEqual(result, None)

    #przekątne


    def test_o_win_right_diagonal(self):

        model = Model()
        model.put_mark('o', 0, 2)
        model.put_mark('x', 1, 0)
        model.put_mark('o', 1, 1)
        model.put_mark('x', 2, 1)
        model.put_mark('o', 2, 0)

        result = model.get_winner()

        self.assertEqual(result, 'o')

    def test_x_win_left_diagonal(self):

        model = Model()
        model.put_mark('x', 0, 0)
        model.put_mark('x', 1, 1)
        model.put_mark('x', 2, 2)

        result = model.get_winner()

        self.assertEqual(result, 'x')


class TestIsFree(unittest.TestCase):

    def test_empty_cell(self):
        model = Model()
        model.put_mark('x', 0, 1)

        result = model.is_free(0, 0)

        self.assertEqual(result, True)

    def test_full_cell(self):
        model = Model()
        model.put_mark('x', 0, 0)

        result = model.is_free(0, 0)

        self.assertEqual(result, False)

















